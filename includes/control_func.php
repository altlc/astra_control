<?php
  
function reload_astra($astra_id)
{
    $query = new db_query();
    $astra = $query->assoc_array("select * from astra_instance where astra_id=".intval($astra_id));
    
    $cmd=array('reload' => true );
    
    post_json('http://'.$astra['control_server_addr'].':'.$astra['control_server_port'].'/control/',$cmd);
}



function post_json( $url, $json)
{
        $options = array(
          'http' => array(
            'method'  => 'POST',
            'content' => json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
            'header'=>  "Content-Type: application/json\r\n" .
                        "Accept: application/json\r\n"
            )
        );

        $context  = stream_context_create( $options );
        $result = file_get_contents( $url, false, $context );
        $response = json_decode( $result );
        return $response;
}


  
?>
